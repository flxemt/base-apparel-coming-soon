const form = document.getElementById('form')
const input = document.getElementById('email-input')
const errorText = document.getElementById('error-text')

function handleSubmit(event) {
  event.preventDefault()
  if (!input.value.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)) {
    input.classList.add('form__input--error')
    errorText.style.display = 'block'
    return
  }

  input.classList.remove('form__input--error')
  errorText.style.display = 'none'
}

form.addEventListener('submit', handleSubmit)
